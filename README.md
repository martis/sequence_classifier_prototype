# Introduction

`sequence_classifier` is a general-purpose **LSTM RNN** followed by **projection layers** for **classifying sequences** using **TensorFlow 1.3**

Run `python sequence_classifier.py -h` for help.

# Tutorial - The Spaghetti Club
As a simple example, we train the network on the so-called 'Spaghetti Club' problem: a word is a member of the club if it contains at least one double letter, e.g. 'Spaghetti' is in, 'Chocolate' is out. We use a four-letter alphabet (A, B, C, D).

## Training a model
### Input
This section **explains** what has to be provided to train a model - data, configuration files - and **how to run the script in training mode**.

1. **Prepare training data:** create two datasets, one containing only words with at least one double letters, and one with words without double letters. Alternatively, use the provided **example sets**:
    * `data/spaghetti_data/positive_spaghetti_training_set.txt`
    * `data/spaghetti_data/negative_spaghetti_training_set.txt`
  
    Supported data formats are **FASTA** and **newline-separated**.
2. **Create a .yaml file where you specify which data to use for each class.** For each data class, you have to provide the class label and the path to the data that belongs to this class. You can use the following **example file**:
    * `data/spaghetti_data/spaghetti_training_data_paths.yaml`
    
3. **Create a .yaml file with training parameters:** this has to be a .yaml file where the following options have to be specified. You can use the following **example file**:
    * `configs/training_configs/spaghetti_training.yaml`
    
    There are **two classes of options**: the performance-relevant ones and the ones for saving the model.

    `training_options`: these options affect the performance (convergence) of the training:
    * `batch_size`: the number of examples used for each training cycle.
    * `learning_rate`: the learning rate.
    * `decay_learning_rate`: if 'True', then the learning rate decays exponentially.
    * `decay_lambda`: lambda for the exponential decay.
    * `min_learning_rate`: once the decaying learning rate reaches this value, it is kept constant.
    * `training_cycles`: the number of optimization cycles.
    
    `saving_options`: options for saving - no effect on convergence, but on training speed:
    * `saving_step`: specify after how many cycles you want to save the checkpoints.
    * `plot_saving_step`: specify after how many cycles you want to plot the loss and accuracy per step.
4. **Create a .yaml file with model parameters:** this has to be a .yaml file where the hyperparameters of the models are specified. You can use the following **example file**:
    * `configs/model_configs/spaghetti_model.yaml`
    
    There are options for the **encoder**, for the **embedding**, and for the **projection**:
    
    `encoder_options`:
    * `bidirectional_encoding`: bidirectional encoding if `True`, else forward encoding.
    * `n_hidden`: the size of the internal state vectors and the output vectors.
    * `encoder_activation_function`: the activation function for the gates in the LSTM cell.
    * `c_state_activation_function`: activation function to be applied to the encoder c state, `None` recommended.
    
    `embedding_options`:
    * `embedding_dimension`: the dimension of the embedding - ignored if one-hot encoding is used.
    * `with_one_hot_embedding`: use one-hot encoding instead of a trainable embedding.
    * `use_all_zeros_for_padding`: if one-hot encoding: do not add a dimension for 'PAD', just use a zero vector.
    
    `projection_options`:
    * `classification_activation_function`: activation function after the final projection; `None`, `tanh`, or `elu`.
    * `n_additional_projection_layers`: the number of projection layers between the RNN encoder and the projection to `n_classes`.
    * `intermediate_activation_function`: activation function for the additional projection layers; `None`, `tanh`, or `elu`.
    * For every additional layer, add `layer_i_size` to specify its output size.

5. **Run the script**: the script has to be run in the Terminal using
    
    `python sequence_classifier.py`
    
    The following **positional command line arguments** are **required**:
    * The **mode**, `Train` if we want to train a model.
    * The **model name**, `spaghetti_club` in this example.
    * The **path of the file where the data paths are specified**, `data/spaghetti_training_data_paths.yaml` in this example.
     
    In the training mode, the following additional argument is **required**:
    * The **path of the training options file**, `-tc` or `--training_config_file_path`.
     
    If we create a **new model**, the following additional arguments are **required**:
    * The **path of the model hyperparameters file**, `-mc` or `--model_config_file_path`.
    * The **path to the vocab file**, `-vc` or `--vocabulary_file_path`.
    
    The following options are **optional**:
    * The data format, `-df` or `--data_format`: Default is `FASTA`, the other option is `newline-separated`.
    * The training mode, i.e. if we want to resume training an existing model or create a new one, `-tm` or `--training_mode`. Default is `create_new`.
    * The save mode, i.e. if we want to save the model, `-sm` or `--save_mode`. Default is `True`.
    * The verbosity, i.e. if we want to print **everything**, `-sv` or `--super_verbose`. Default is `False`. Recommended if you want to see what happens at each step in the model.
    
    **To train the Spaghetti Club model, use the following command:**

    `python sequence_classifier.py spaghetti_club Train data/spaghetti_training_data_paths.yaml -mc=configs/model_configs/spaghetti_model.yaml -tc=configs/training_configs/spaghetti_training.yaml -vc=data/spaghetti_data/vocab.yaml -df=newline-separated`
    
    **Notes**:
    * If you set `-tm` to `resume`, but the model does not exist yet, a new one will be created.
    * If you set `-tm` to `create_new`, but the model does already exist, a new model `model_name_i` will be created.
6. **Monitor the progress with TensorBoard**: 
    1. Run the following to monitor the training process:
    
        `tensorboard --logdir=models/spaghetti_club/tensorboard_data/tensorboard_data_resume_0`
    2. Open http://localhost:6006 in your browser.
    
### Output
This section **explains what is produced when you run the script in training mode**.

If you run the script in training mode, a directory `'model_name'` is **automatically created** (this does **not** happen if you **resume** training of an **existing model**).

The directory `'model_name'` contains the following files and subdirectories:
* A file `model_name_saved_model_parameters.yaml` containing the **model's hyperparameters**. In test/inference mode or when the training is resumed, the parameters will be loaded from this file (thus in this case the argument `-mc` is not required when running the script). **Do not change this file!**
* A file `training_resumes_tracker.txt`: this file helps counting how many times training has been resumed. **Do not change this file!**
* A directory `checkpoints`: contains the **saved model weights**.
* A directory `tensorboard_data`: contains the **TensorBoard summaries**.
* A directory `training_configs`: contains a copy of the **training parameters** used in each resume.
* A directory `training_data`: contains the **training data** used for training the model.
* A directory `plots`: contains plots of the loss and accuracy per training cycle.
* A directory `timing`: contains information about the time taken for running the script.

## Testing a model
1. **Prepare test data:** create two datasets, one containing only words with at least one double letters, and one with words without double letters. Alternatively, use the provided **example sets**:
    * `data/spaghetti_data/positive_spaghetti_test_set.txt`
    * `data/spaghetti_data/negative_spaghetti_test_set.txt`
  
    Supported data formats are **FASTA** and **newline-separated**.
2. **Create a .yaml file where you specify which data to use for each class.** For each data class, you have to provide the class label and the path to the data that belongs to this class. You can use the following **example file**:
    * `data/spaghetti_data/spaghetti_test_data_paths.yaml`
3. **Run the script in test mode**: the script has to be run in the Terminal using
    
    `python sequence_classifier.py`
    
    The following **positional command line arguments** are **required**:
    * The **mode**, `Test` if we want to train a model.
    * The **model name**, `spaghetti_club` in this example.
    * The **path of the file where the data paths are specified**, `data/spaghetti_test_data_paths.yaml` in this example.

    
    The following options are **optional**:
    * The data format, `-df` or `--data_format`: Default is `FASTA`, the other option is `newline-separated`.
    * The verbosity, i.e. if we want to print **everything**, `-sv` or `--super_verbose`. Default is `False`. Recommended if you want to see what happens at each step in the model.
    
    **To test the Spaghetti Club model, use the following command:**

    `python sequence_classifier.py spaghetti_club Test data/spaghetti_test_data_paths.yaml -df=newline-separated`

## Use the model for inference
**Run the script in test mode**: the script has to be run in the Terminal using
    
`python sequence_classifier.py`
    
The following **positional command line arguments** are **required**:
* The **mode**, `Infer` if we want to use a model to classify data.
* The **model name**, `spaghetti_club` in this example.
* The **path of the data to be classified**.

    
The following options are **optional**:
* The data format, `-df` or `--data_format`: Default is `FASTA`, the other option is `newline-separated`.
* The output format, `-of` or `--output_format`: uses the same format as specified by `-df` if not given. Available formats: `FASTA` and `newline-separated`.
* The verbosity, i.e. if we want to print **everything**, `-sv` or `--super_verbose`. Default is `False`. Recommended if you want to see what happens at each step in the model.

**To use the Spaghetti Club model to classify data, use the following command:**

`python sequence_classifier.py spaghetti_club Infer 'path_to_data'`


   