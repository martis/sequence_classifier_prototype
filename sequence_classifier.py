import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell, rnn_cell_impl
import numpy as np
import string
import itertools
import yaml
import argparse
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(color_codes=True)
sns.set_style('darkgrid', {'axes.facecolor': '.95'})
import time
import os

from utilities import funcs
from utilities import parser
from utilities import saver as svr # This is to avoid name clashing later

if __name__ == '__main__':

    # Models parent folder
    models_parent_folder = 'models/'

    # Activation functions
    activation_functions = {'tanh' : tf.tanh, 'elu' : tf.nn.elu}

    # Parse command line arguments
    arg_parser = argparse.ArgumentParser(description='Sequence classification model.')
    arg_parser.add_argument('model_name', type=str, help='Specify model name.')
    arg_parser.add_argument('mode', type=str, choices=['Train', 'Test', 'Infer'],
                            help='Run script in training, test, or inference mode.')
    arg_parser.add_argument('data', type=str, help='In training or test mode: specify a .yaml \
                            file with the paths of the data of each class. In inference mode: \
                            specify the path to the data (FASTA format) you want to classify.')
    arg_parser.add_argument('-vc', '--vocabulary_file_path', type=str, default=None,
                            help='If in training mode and creating a new model: specify a \
                            vocabulary file.')
    arg_parser.add_argument('-mc', '--model_config_file_path', type=str, default=None,
                            help='Specify a model config file path if in training mode and the model \
                            is created from scratch.', metavar='')
    arg_parser.add_argument('-tc', '--training_config_file_path', type=str, default=None,
                            help='Specify a training config file path if in training mode.', metavar='')
    arg_parser.add_argument('-tm', '--training_mode', type=str, choices=['resume', 'create_new'], default='create_new',
                            help='Specify whether you want to start training from scratch or continue \
                            training an existing model. Default is to create a new model.')
    arg_parser.add_argument('-sm', '--save_model', type=parser.str2bool,
                            help='In training mode: choose whether you want to save your model or not.\
                            Default is to save the model.', default='True')
    arg_parser.add_argument('-sv', '--super_verbose', type=parser.str2bool,
                            help='In training mode: print everything?.\
                            Default is no.', default='n')
    arg_parser.add_argument('-df', '--data_format', type=str, help='The format of the input data. \
                            Default is FASTA format.', choices=['FASTA', 'newline-separated'], default='FASTA')
    arg_parser.add_argument('-of', '--output_format', type=str, help='The format of the inference output. \
                            Default format is copied from -df.', choices=['FASTA', 'newline-separated'], default=None)
    args = arg_parser.parse_args()

    # Training/testing/inference mode, verbosity level
    mode = args.mode
    super_verbose = args.super_verbose

    # Get the data
    data_path = args.data
    data_format = args.data_format
    output_format = args.output_format
    data, class_labels, output_format = parser.get_data(mode=mode, data_path=data_path, data_format=data_format, output_format=output_format)

    # Prepare the model directories
    save_model = args.save_model
    model_name = args.model_name
    training_mode = args.training_mode
    model_name, model_path, resume_number, checkpoints_path, tensor_board_summary_path, saved_model_parameters_path = \
        svr.prepare_model_folder(model_name, mode, training_mode, save_model, class_labels, data, models_parent_folder)
    print('Model name:\t\t', model_name)
    print('Model path:\t\t', model_path)
    print('Checkpoints path:\t', checkpoints_path)
    print('TensorBoard path:\t', tensor_board_summary_path)
    print('Resume number\t\t', resume_number, '\n')

    # Check if required files are available
    if mode == 'Train':
        model_config_file_path = args.model_config_file_path
        training_config_file_path = args.training_config_file_path
        vocabulary_file_path = args.vocabulary_file_path
        assert training_config_file_path is not None, \
            'You must specify a training config file in training mode!'
        if resume_number == 0 and not os.path.isfile(saved_model_parameters_path): # If the model already exists, the model parameters will be loaded from an automatically generated config file.
            assert model_config_file_path is not None, \
                'You must specify a model config file in training mode when creating a new model!'
            assert vocabulary_file_path is not None, \
                'You must specify a vocabulary file in training mode when creating a new model!'

    # Load YAML file with model and training parameters
    if mode == 'Train' and not os.path.isfile(saved_model_parameters_path):
        print('Loading model parameters and vocabulary from config files...')
        # Load the parameters
        with open(model_config_file_path, 'r') as stream:
            model_parameters = yaml.safe_load(stream)
        # Add the vocabularies
        with open(vocabulary_file_path, 'r') as stream:
            vocab = yaml.safe_load(stream)
        model_parameters['vocabulary'] = vocab
        # Add the number of classes
        n_classes = len(class_labels)
        model_parameters['classification'] = {'number_of_classes' : n_classes}
        class_indices = np.arange(len(class_labels)) # These are the class indices (ints) used in the models
        model_parameters['class_labels'] = {class_labels[i] : str(i) for i in class_indices} # str for YAML dump
        class_labels_dict = {class_labels[i] : i for i in class_indices} # This is important: here we set which index actually belongs to which class!
    else:
        print('Loading model parameters and vocabulary from saved model parameters file...')
        with open(saved_model_parameters_path, 'r') as stream:
            model_parameters = yaml.safe_load(stream)
    if mode == 'Train':
        # Load the training parameters; we do this for each resume, so that the parameters can be adjusted for each new run.
        with open(training_config_file_path, 'r') as stream:
            training_parameters = yaml.safe_load(stream)

    # Some control switches, vocabulary creation, and automatic option setting
    vocab, vocab_size, model_parameters = \
        funcs.check_and_adjust_options(mode=mode,
                                       save_model=save_model,
                                       model_parameters=model_parameters,
                                       activation_functions=activation_functions,
                                       saved_model_parameters_path=saved_model_parameters_path)

    # After all compatibility checks done, display and save the parameters
    print('\n\n~~~ Model parameters summary ~~~\n')
    for key in model_parameters:
        print(key)
        for subkey in model_parameters[key]:
            print('\t', subkey, model_parameters[key][subkey])
        print()
    if mode == 'Train':
        print('\n\n~~~ Training parameters summary ~~~\n')
        for key in training_parameters:
            print(key)
            for subkey in training_parameters[key]:
                print('\t', subkey, training_parameters[key][subkey])
            print()

    # Write the parameters to a file for testing and inference
    if mode == 'Train' and save_model:
        svr.save_model_parameters(model_path, model_name, resume_number, training_parameters, model_parameters)



    # ~~~ Now before we start building the model, we load all parameters ~~~ #

    bidirectional_encoding = model_parameters['encoder_options']['bidirectional_encoding']
    n_hidden_encoder = model_parameters['encoder_options']['n_hidden']
    encoder_activation_function = activation_functions.get(model_parameters['encoder_options']['encoder_activation_function'])
    c_state_activation_function = model_parameters['encoder_options']['c_state_activation_function']
    embedding_dimension = model_parameters['embedding_options']['embedding_dimension']
    with_one_hot_input_embedding = model_parameters['embedding_options']['with_one_hot_input_embedding']
    use_all_zeros_for_padding = model_parameters['embedding_options']['use_all_zeros_for_padding']
    if mode == 'Train' and not os.path.isfile(saved_model_parameters_path):
        pass # We have already defined 'n_classes', 'class_labels_dict_raw', and 'class_labels_dict' in this case.
    else:
        n_classes = model_parameters['classification']['number_of_classes']
        class_labels_dict_raw = model_parameters['class_labels']
        class_labels_dict = {key : int(class_labels_dict_raw[key]) for key in class_labels_dict_raw}
    if mode == 'Train':
        batch_size = training_parameters['training_options']['batch_size']
        assert batch_size <= n_classes * min(len(data[key]) for key in data), \
            'batch_size can not be larger than n_classes times the size of the smallest dataset!'
        learning_rate = training_parameters['training_options']['learning_rate']
        decay_learning_rate = training_parameters['training_options']['decay_learning_rate']
        if decay_learning_rate:
            learning_rate_0 = learning_rate
        decay_lambda = training_parameters['training_options']['decay_lambda']
        min_learning_rate = training_parameters['training_options']['min_learning_rate']
        training_cycles = training_parameters['training_options']['training_cycles']
        saving_step = training_parameters['saving_options']['saving_step']
        plot_saving_step = training_parameters['saving_options']['plot_saving_step']
    elif mode == 'Test':
        batch_size = n_classes * min(len(data[key]) for key in data) # Ensure that things are balanced!
    elif mode == 'Infer':
        batch_size = len(data)
    projection_options = model_parameters['projection_options']
    intermediate_activation_function = model_parameters['projection_options']['intermediate_activation_function']
    classification_activation_function = model_parameters['projection_options']['classification_activation_function']
    n_additional_projection_layers = projection_options.get('n_additional_projection_layers')
    projection_layers_keys = ['layer_' + str(i + 1) + '_size' for i in range(n_additional_projection_layers)]
    projection_layers_sizes = [projection_options.get(key) for key in projection_layers_keys]



    # ~~~ We can already prepare the embedding matrix if we do not want a trainable embedding ~~~ #

    if with_one_hot_input_embedding:
        embedding_matrix = funcs.get_one_hot_embedding_matrix(vocab_size, use_all_zeros_for_padding)





    # ~~~~~ Now we build the model ~~~~~ #

    # We need a placeholder for the inputs; we will feed the inputs in batch-major form,
    # this is slightly less efficient than time-major, but it is more 'natural'.
    # The shape of the input is thus [batch_size, max_steps]. However, since 'max_steps'
    # depends on the batch, we do not yet specify it here, but instead use 'None'.
    # The inputs at this stage are a dense representation of the input sequences,
    # i.e. a vector of symbol indices for each sequence.
    encoder_inputs_placeholder = tf.placeholder(tf.int32, shape=[batch_size, None], name='encoder_inputs')

    # Encoder input lengths placeholder (so that the encoder knows where to stop):
    encoder_lengths_placeholder = tf.placeholder(tf.int32, shape=[batch_size], name='sequence_lengths')

    # Now we encode the sources:
    if with_one_hot_input_embedding:
        # We already have the embedding matrix; we use a placeholder for creating the graph, then feed the embedding matrix when running it.
        embedding_matrix_to_encoder = tf.placeholder(tf.float32, shape=[vocab_size, embedding_dimension], name='one_hot_matrix')
    else:
        # Embedding matrix as trainable variable: we have to create the variable.
        embedding_matrix_to_encoder = tf.Variable(tf.random_uniform([vocab_size, embedding_dimension], -1.0, 1.0), name='trainable_embedding_matrix')

    # Embedd inputs
    embedded_encoder_inputs = tf.nn.embedding_lookup(embedding_matrix_to_encoder, encoder_inputs_placeholder)

    # Encoder cells
    # Check https://github.com/tensorflow/tensorflow/blob/r1.3/tensorflow/python/ops/rnn_cell_impl.py#L417 for more options.
    forward_encoder_cell = rnn_cell.LSTMCell(num_units=n_hidden_encoder, initializer=None, activation=encoder_activation_function)
    if bidirectional_encoding:
        backward_encoder_cell = rnn_cell.LSTMCell(num_units=n_hidden_encoder, initializer=None, activation=encoder_activation_function)

    # Now run the RNN
    if bidirectional_encoding:
        # See https://github.com/tensorflow/tensorflow/blob/r1.3/tensorflow/python/ops/rnn.py#L314 for more options.
        encoder_outputs, encoder_states = rnn.bidirectional_dynamic_rnn(
                            forward_encoder_cell,
                            backward_encoder_cell,
                            embedded_encoder_inputs,
                            sequence_length=encoder_lengths_placeholder,
                            dtype=tf.float32,
                            time_major=False)
    else:
        # See https://github.com/tensorflow/tensorflow/blob/r1.3/tensorflow/python/ops/rnn.py#L443 for more options.
        encoder_outputs, encoder_states = rnn.dynamic_rnn(
                            forward_encoder_cell,
                            embedded_encoder_inputs,
                            sequence_length=encoder_lengths_placeholder,
                            dtype=tf.float32,
                            time_major=False)

    # If we use a bidirectional encoder, we have to concatenate state tuples since
    # the bidirectional RNN gives two states: the forward- and the backward
    # state. They have to be concatenated before proceeding to the classification.
    if bidirectional_encoding:
        concatenated_encoder_final_states = tf.concat(encoder_states, axis=-1)
    else:
        concatenated_encoder_final_states = encoder_states

    # Since LSTM networks have two states (the internal state (c) a.k.a
    # the memory, and the state that is also the last output (h)), stored
    # as concatenated_encoder_final_states = (c, h), we have to separate them.
    encoder_c_state = concatenated_encoder_final_states[0]
    encoder_h_state = concatenated_encoder_final_states[1]

    # If we activate this:
    if c_state_activation_function != 'None':
        if c_state_activation_function == 'tanh':
            encoder_c_state = tf.tanh(encoder_c_state)
        elif c_state_activation_function == 'elu':
            encoder_c_state = tf.nn.elu(encoder_c_state)

    # NOTE: here you could add more layers of the form
    # out_i = W_(i, i-1) * out_(i-1) to make the net
    # deeper...
    encoder_output_size = n_hidden_encoder
    if bidirectional_encoding:
        encoder_output_size = 2 * encoder_output_size # We concatenated forward and backward states

    projection_layers_sizes = [encoder_output_size] + projection_layers_sizes
    intermediate_state = encoder_c_state
    for i in range(n_additional_projection_layers):
        projection_matrix = tf.Variable(tf.random_uniform([projection_layers_sizes[i], projection_layers_sizes[i+1]]), name='projection_matrix_' + str(i))
        if intermediate_activation_function == 'elu':
            intermediate_state = tf.nn.elu(tf.matmul(intermediate_state, projection_matrix))
        elif intermediate_activation_function == 'tanh':
            intermediate_state = tf.nn.tanh(tf.matmul(intermediate_state, projection_matrix))
        elif intermediate_activation_function == 'None':
            intermediate_state = tf.matmul(intermediate_state, projection_matrix)

    # Now we want to use the c state for classification.
    # For this, we have to project it to the number of classes,
    # i.e. we have to multiply it by a matrix of shape
    # 2 * n_hidden_encoder x n_classes. (We have 2 * n_hidden_encoder
    # because we have concatenated the two c state vectors from the
    # forward and backward cells, each of size n_hidden_encoder).
    # The projection matrix has to be trainable!
    #projection_matrix = tf.Variable(tf.random_uniform([2 * n_hidden_encoder, n_classes]), name='projection_matrix')
    projection_matrix = tf.Variable(tf.random_uniform([projection_layers_sizes[-1], n_classes]), name='projection_matrix')

    # Now we get our predictions:
    # NOTE: you could also introduce a bias here; this depends
    # also a bit on your choice of the activation function. Here
    # I use tanh, because it is symmetric, and we won't face
    # problems with exploding gradients because we only take it
    # once. So if your set is balanced, the bias should go to
    # zero. However, if you want to use ELUs (exponential linear
    # units), you might have to introduce a bias. ELUs are nice
    # because they don't suffer so much from exploding gradients.
    # 'logits' is now a matrix of size batch_size x n_classes, and
    # the entries are logits, i.e. not yet probabilities! We will
    # normalize them when we compute the loss (in training mode)
    # or in a later step (testing/inference modes).
    #logits = tf.tanh(tf.matmul(encoder_c_state, projection_matrix))
    if classification_activation_function == 'tanh':
        logits = tf.tanh(tf.matmul(intermediate_state, projection_matrix))
    elif classification_activation_function == 'elu':
        logits = tf.elu(tf.matmul(intermediate_state, projection_matrix))
    elif classification_activation_function == 'None':
        logits = tf.matmul(intermediate_state, projection_matrix)

    # Now get the argmax to get the actual predictions
    # NOTE: we will use the unscaled logits to compute
    # the loss! This is just for monitoring.
    logits_argmax = tf.argmax(logits, axis=1)

    # In training: we have to compute the cost and minimize it.
    # Since the classes are mutually exclusive, we use
    # 'tf.nn.sparse_softmax_cross_entropy_with_logits'
    # (https://www.tensorflow.org/api_docs/python/tf/nn/sparse_softmax_cross_entropy_with_logits)
    if mode == 'Train' or mode == 'Test':
        # This can not be done in 'Infer' mode since we don't know the targets!
        # We have to create a placeholder for the targets
        target_labels_placeholder = tf.placeholder(tf.int32, shape=[batch_size], name='target_labels')
        # We can also compute the accuracy for monitoring:
        logits_argmax = tf.cast(logits_argmax, tf.int32)
        correct_pred = tf.equal(logits_argmax, target_labels_placeholder)
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    if mode == 'Train': # We do not want to do this in testing mode, we just need the accuracy there.
        # We compute the loss for each sample
        losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels = target_labels_placeholder,
            logits = logits)

        # Get avg. loss
        loss = tf.reduce_sum(losses) / tf.to_float(
            tf.reduce_sum(encoder_lengths_placeholder))

        # Now we optimize
        learning_rate_placeholder = tf.placeholder(tf.float32, shape=[], name='learning_rate')
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate_placeholder).minimize(loss) # Adam Optimizer

        # Collection to run sess only once.
        # NOTE: if you run two sessions, you might not get the same results as the
        # variables are initialized twice... So for seriously training the model,
        # i.e. with 'super_verbose' turned off, we will only run this OP!
        optimizer_op_collection = (optimizer, loss, accuracy, logits, logits_argmax)

        # We want to monitor the loss and the accuracy with TensorBoard
        tf.summary.scalar('Loss', optimizer_op_collection[1])
        tf.summary.scalar('Accuracy', optimizer_op_collection[2])

    # We want to save our model, so we create a 'saver' node.
    # NOTE: the saver node is also used for loading trained models!
    saver = tf.train.Saver()  # defaults to saving all variables
    # We also want summaries for TensorBoard to monitor the training:
    if mode == 'Train':
        merged = tf.summary.merge_all()





    # ~~~~~ Now we run the session ~~~~~ #

    # Time the optimization session
    optimization_starttime = time.time()

    init = tf.global_variables_initializer()
    with tf.Session() as sess:

        # First, we get the saved weights in case we want to resume a training
        # or run the script in 'Test' or 'Infer' mode.
        stop_run = False # Will be set to true if 'Test' or 'Infer' mode and no checkpoints found
        init_variables = True # Will automatically be set to 'False' if training is resumed
        print('Load checkpoints file if they exist...')
        ckpt = tf.train.get_checkpoint_state(checkpoints_path)
        latest_cycle = 0
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            init_variables = False
            if mode == 'Train':
                print('Checkpoints found, resume training from there.\n')
                latest_cycle = int(tf.train.latest_checkpoint(checkpoints_path).split('model.ckpt-')[-1])
        else:
            if mode == 'Infer' or mode == 'Test':
                print('No checkpoint found -> stop!\n')
                stop_run = True
            else:
                print('No checkpoint found -> start training from scratch.\n')

        # Start the model training or evaluation
        if not stop_run:

            # Being nice: give people the command to run TensorBoard :)
            if mode == 'Train' and save_model:
                print('To monitor the training progress with TensorBoard, run\ntensorboard --logdir=' + models_parent_folder \
                    + model_name + '/tensorboard_data/tensorboard_data_resume_' + str(resume_number) + '\n')

            # Launch the SummaryWriter for TensorBoard
            if mode == 'Train' and save_model:
                losses_list = []        # We collect losses and accuracies for plotting the progress
                accuracies_list = []    # independently of TensorBoard
                summary_writer = tf.summary.FileWriter(tensor_board_summary_path)

            # Initialize all variables
            if mode == 'Train' and init_variables:
                sess.run(init) # All the trainable stuff gets random weights assigned

            cycle = latest_cycle
            if mode == 'Test' or mode == 'Infer':
                cycle = 0
                training_cycles = 1

            # Now the optimization loop begins!
            while cycle < training_cycles + latest_cycle:

                cycle = cycle + 1

                if super_verbose:
                    print('\n\nCycle #', cycle, '\n')

                # First we sample a minibatch:
                if mode == 'Train' or mode == 'Test':
                    minibatch_sources, minibatch_targets, minibatch_lengths, n_steps = funcs.get_minibatch(class_labels_dict, data, batch_size)
                else:
                    minibatch_sources, minibatch_lengths, ordered_sample_labels, n_steps  = funcs.prepare_data_for_inference(data)
                    minibatch_targets = None

                # Now we have to feed the placeholders with actual data.
                # The input sequences ('minibatch_sources') have to be converted
                # to dense label matrices (a vector of symbol indices for each
                # sequence), and we have to pad them to the same length.
                dense_encoder_inputs = funcs.get_dense_representation(minibatch_sources, vocab, n_steps)

                if super_verbose:
                    print('Data in minibatch:')
                    print(minibatch_sources)
                    print('\nDense representation of the encoder inputs:')
                    print(dense_encoder_inputs)

                # Now we specify which placeholder we want to feed with these inputs
                encoder_input_feed_dict = {encoder_inputs_placeholder : dense_encoder_inputs}

                # We also have to provide the sequence lengths so the encoder 'knows' when to stop.
                encoder_lengths_feed_dict = {encoder_lengths_placeholder : minibatch_lengths}
                if super_verbose:
                    print('\nLengths of the inputs:')
                    print(minibatch_lengths)

                # We have to provide the one-hot embedding matrix
                if with_one_hot_input_embedding:
                    embedding_feed_dict = {embedding_matrix_to_encoder : embedding_matrix}
                else:
                    embedding_feed_dict = {} # If the embedding is trainable, we don't have to provide the matrix

                # Provide the targets
                if mode == 'Train' or mode == 'Test':
                    target_labels_feed_dict = {target_labels_placeholder : np.array(minibatch_targets)}
                    if super_verbose:
                        print('\nMinibatch targets:')
                        print(minibatch_targets)
                else:
                    target_labels_feed_dict = {}

                # The learning rate - only used in training mode
                if mode == 'Train':
                    if decay_learning_rate:
                        learning_rate = funcs.learning_rate_decay(cycle, learning_rate_0, decay_lambda, min_learning_rate)
                    learning_rate_feed_dict = {learning_rate_placeholder : learning_rate}
                    if super_verbose:
                        print('\nLearning rate:')
                        print(learning_rate)
                else:
                    learning_rate_feed_dict = {}

                # Merge all feeds
                feed = dict(itertools.chain(
                    encoder_input_feed_dict.items(),
                    encoder_lengths_feed_dict.items(),
                    target_labels_feed_dict.items(),
                    embedding_feed_dict.items(),
                    learning_rate_feed_dict.items()))


                # The embedded inputs (only when if want to print them):
                if super_verbose:
                    emb_inp = sess.run(embedded_encoder_inputs, feed_dict=feed)
                    print('\nEncoded/embedded encoder inputs:')
                    print(emb_inp)

                # Run the encoder (only when if want to print the outputs/states):
                if super_verbose:
                    enc_outs, enc_states, enc_c_state = sess.run([encoder_outputs, encoder_states, encoder_c_state], feed_dict=feed)
                    print('\nEncoder outputs:')
                    if bidirectional_encoding:
                        fw_enc_outs = enc_outs[0]
                        bw_enc_outs = enc_outs[1]
                        fw_enc_states = enc_states[0]
                        bw_enc_states = enc_states[1]
                        for i in range(batch_size):
                            print('Sequence #', i, ': ' + minibatch_sources[i])
                            print('Forward:')
                            print(fw_enc_outs[i])
                            print('Backward:')
                            print(bw_enc_outs[i])
                            print()
                    else:
                        for i in range(batch_size):
                            print('Sequence #', i, ': ' + minibatch_sources[i])
                            print(enc_outs[i])
                            print()

                    print('\nEncoder states:')
                    if bidirectional_encoding:
                        print('Forward c states:')
                        print(fw_enc_states[0])
                        print('Forward h states:')
                        print(fw_enc_states[1])
                        print('Backward c states:')
                        print(bw_enc_states[0])
                        print('Backward h states:')
                        print(bw_enc_states[1])
                    else:
                        print('Encoder c state:')
                        print(enc_states[0])
                        print('Encoder h state:')
                        print(enc_states[1])

                    print('\nConcatenated encoder c states:')
                    print(enc_c_state)

                # If we are in the training mode, we have to run the optimizer.
                if mode == 'Train':
                    op_op = sess.run(optimizer_op_collection, feed_dict=feed)
                    if super_verbose:
                        print('\nLogits:')
                        print(op_op[3])
                        print('\nPredictions:')
                        print(op_op[4])
                        print('\nTargets:')
                        print(np.array(minibatch_targets))
                        print('\nLoss:')
                        print(op_op[1])
                        print('\nAccuracy:')
                        print(op_op[2])
                    else:
                        loss = op_op[1]
                        accuracy = op_op[2]
                        print('Cycle: ' + str(cycle) + '\tLoss: ' + str(np.round(loss, 4)) + '\tAccuracy: ' + str(np.round(accuracy, 3)))
                        if save_model:
                            losses_list.append(loss)
                            accuracies_list.append(accuracy)


                    if cycle % saving_step == 0 and save_model:
                        # This saves the model weights:
                        saver.save(sess, checkpoints_path + 'model.ckpt', global_step=cycle)
                        # Write summary for TensorBoard:
                        summary_str = sess.run(merged, feed_dict=feed)
                        summary_writer.add_summary(summary_str, cycle)
                        # Plot the losses and accuracies over time
                        if cycle % plot_saving_step == 0:
                            # Losses
                            plot_name = model_path + 'plots/loss_per_cycle_resume_' + str(resume_number) +'.pdf'
                            plt.figure(1)
                            plt.subplot(211)
                            plt.title('Loss per step')
                            plt.plot(losses_list)
                            plt.savefig(plot_name, bbox_inches='tight', format='pdf', dpi=300)
                            plt.close()
                            # Accuracies
                            plot_name = model_path + 'plots/accuracy_per_cycle_resume_' + str(resume_number) +'.pdf'
                            plt.figure(1)
                            plt.subplot(211)
                            plt.title('Accuracy per step')
                            plt.plot(accuracies_list)
                            plt.savefig(plot_name, bbox_inches='tight', format='pdf', dpi=300)
                            plt.close()

                # In testing mode, we want to evaluate the performance
                elif mode == 'Test':
                    pred, acc = sess.run([logits_argmax, accuracy], feed_dict=feed)
                    inverse_class_labels_dict = {class_labels_dict[key] : key for key in class_labels_dict}
                    #for i in range(len(minibatch_sources)):
                    #    print('Source: ' + minibatch_sources[i] + '\tPredicted class: ' + str(inverse_class_labels_dict[pred[i]])
                    #          + '\tReal class: ' + str(inverse_class_labels_dict[minibatch_targets[i]]))
                    print('\nAccuracy: ' + str(np.round(acc, 3)) + '\n')

                # In inference mode, we want to classify data
                elif mode == 'Infer':
                    inverse_class_labels_dict = {class_labels_dict[key] : key for key in class_labels_dict}
                    pred = sess.run(logits_argmax, feed_dict=feed)
                    #for i in range(len(minibatch_sources)):
                    #    print('Source: ' + minibatch_sources[i] + '\tPredicted class: ' + str(inverse_class_labels_dict[pred[i]]))
                    if output_format == 'FASTA':
                        string_to_print = '\n'.join(['>' + ordered_sample_labels[_] + '\n' + str(inverse_class_labels_dict[pred[_]]) \
                                                     for _ in range(len(minibatch_sources))])
                    elif output_format == 'newline-separated':
                            string_to_print = '\n'.join([str(inverse_class_labels_dict[pred[_]]) for _ in range(len(minibatch_sources))])
                    with open(model_path + '/inference_out.txt', 'w') as output_file:
                        output_file.write('# Data path: ' + data_path + '\n')
                    with open(model_path + '/inference_out.txt', 'a') as output_file:
                        output_file.write(string_to_print)


    # Timing
    optimization_endtime = time.time()
    optimization_delta = optimization_endtime - optimization_starttime
    converted_time = time.strftime('%H:%M:%S', time.gmtime(optimization_delta))
    print('Optimization session complete; elapsed time: ' + str(converted_time))
    if save_model:
        if mode == 'Train':
            with open(model_path + '/timing/timing_resume_' + str(resume_number)  + '.txt', 'w') as output_file:
                output_file.write('Optimization time:\t' + str(converted_time))
        else:
            with open(model_path + '/timing/timing_inference.txt', 'w') as output_file:
                output_file.write('Inference time:\t' + str(converted_time))
