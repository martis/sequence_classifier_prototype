import numpy as np
import string
import itertools
import tensorflow as tf
import os

def get_one_hot_embedding_matrix(vocab_size, use_all_zeros_for_padding):
    """Returns the identity matrix of vocab_size x vocab_size in case of
    'use_all_zeros_for_padding' == False, else the identity matrix of
    (vocab_size - 1) x (vocab_size - 1) stacked on top of a zero vector
    of vocab_size - 1"""
    if use_all_zeros_for_padding:
        embedding_matrix = np.vstack((np.identity(vocab_size - 1),
        np.array([0 for _ in range(vocab_size - 1)])))
    else:
        embedding_matrix = np.identity(vocab_size)
    return embedding_matrix

def get_minibatch(class_labels_dict, data, batch_size):
    """Returns a minibatch of size batch_size // n_classes. Returned
    are the souces, targets, lengths, and the length of the longest
    sequence in the minibatch."""
    n_classes = len(class_labels_dict)
    ordered_labels = list(sorted([label for label in class_labels_dict]))
    samples_per_class = int(batch_size // n_classes)
    minibatch_sources_nested = [[data[label][_] for _ in np.random.choice(range(len(data[label])), size=samples_per_class, replace=False)] \
        for label in ordered_labels]
    minibatch_sources = [val for sublist in minibatch_sources_nested for val in sublist]
    minibatch_targets = np.concatenate([class_labels_dict[label] * np.ones(samples_per_class, dtype=np.int) for label in ordered_labels])
    minibatch_lengths = np.array([len(sequence) for sequence in minibatch_sources])
    n_steps = max(minibatch_lengths)
    return minibatch_sources, minibatch_targets, minibatch_lengths, n_steps

def prepare_data_for_inference(data):
    """Takes a dictionary from a .FASTA file and returns a list of
    sequences, a list of their lengths, an ordered list of the
    sequence names, and the length of the longest sequence."""
    ordered_sample_labels = list(sorted([label for label in data]))
    minibatch_sources = [data[label] for label in ordered_sample_labels]
    minibatch_lengths = np.array([len(sequence) for sequence in minibatch_sources])
    n_steps = max(minibatch_lengths)
    return minibatch_sources, minibatch_lengths, ordered_sample_labels, n_steps

def get_dense_representation(minibatch_sources, vocab, n_steps):
    """Returns a dense matrix representation of a list of sequences
    in batch-major form, i.e. each row is a vector representation of a
    sequence."""
    return np.array([np.array([int(vocab.get(letter)) for letter in sequence] # The sequence
        + [vocab.get('PAD') for _ in range(n_steps - len(sequence))]) # Padding
        for sequence in minibatch_sources])

def learning_rate_decay(step, learning_rate_0, lambd, min_learning_rate):
    """Returns the exponentially decaying learning rate."""
    return (learning_rate_0 - min_learning_rate) * np.exp(-step / lambd) + min_learning_rate

# Automatic option setting
def check_and_adjust_options(mode, save_model, model_parameters, activation_functions, saved_model_parameters_path):
    # Input vocabulary
    vocab = model_parameters['vocabulary']
    if mode == 'Train' and not os.path.isfile(saved_model_parameters_path):
        vocab['PAD'] = len(vocab)
    vocab_size = len(vocab)
    if model_parameters['embedding_options']['with_one_hot_input_embedding']:
        if model_parameters['embedding_options']['use_all_zeros_for_padding']:
            model_parameters['embedding_options']['embedding_dimension'] = vocab_size - 1
        else:
            model_parameters['embedding_options']['embedding_dimension'] = vocab_size
    # Activation functions
    if model_parameters['encoder_options']['encoder_activation_function'] != 'tanh' \
        and model_parameters['encoder_options']['encoder_activation_function'] != 'elu':
        # TODO: allow more options
        print('Available encoder activation functions: tanh and elu. Your choice is not available, will be set to tanh automatically.')
        model_parameters['encoder_options']['encoder_activation_function'] = 'tanh'
    if model_parameters['encoder_options']['c_state_activation_function'] != 'tanh' \
        and model_parameters['encoder_options']['c_state_activation_function'] != 'elu' \
        and model_parameters['encoder_options']['c_state_activation_function'] != 'None':
        # TODO: allow more options
        print('Available c-state activation functions: None, tanh, and elu. Your choice is not available, will be set to None automatically.')
        model_parameters['encoder_options']['c_state_activation_function'] = 'None'
    if model_parameters['projection_options']['intermediate_activation_function'] != 'tanh' \
        and model_parameters['projection_options']['intermediate_activation_function'] != 'elu' \
        and model_parameters['projection_options']['intermediate_activation_function'] != 'None':
        # TODO: allow more options
        print('Available intermediate activation functions: None, tanh, and elu. Your choice is not available, will be set to elu automatically.')
        model_parameters['projection_options']['intermediate_activation_function'] = 'elu'
    if model_parameters['projection_options']['classification_activation_function'] != 'tanh' \
        and model_parameters['projection_options']['classification_activation_function'] != 'elu' \
        and model_parameters['projection_options']['classification_activation_function'] != 'None':
        # TODO: allow more options
        print('Available classification activation functions: None, tanh, and elu. Your choice is not available, will be set to None automatically.')
        model_parameters['projection_options']['classification_activation_function'] = 'None'
    return vocab, vocab_size, model_parameters

if __name__ == '__main__':

    # Usage example: embedding_matrix
    print(get_one_hot_embedding_matrix(vocab_size=4, use_all_zeros_for_padding=False))
    print(get_one_hot_embedding_matrix(vocab_size=4, use_all_zeros_for_padding=True))

    # Usage example: minibatch
    test_data = {'0' : ['0000', '00', '000'], '1': ['11', '11111', '111'], '2' : ['222', '2', '22']}
    class_labels_dict = {'0' : 1, '1' : 0, '2' : 2}
    sources, targets, lengths, n_steps = get_minibatch(class_labels_dict, test_data, 7)
    print(sources)
    print(targets)
    print(lengths)
    print(n_steps)
