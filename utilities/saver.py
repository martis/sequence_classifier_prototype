"""Prepare folders to save models, TensorBoard summary and model parameters"""

import os
import string
import random
import numpy as np
import re
import yaml

def write_files_with_training_data(model_path, class_labels, data, resume_number):
    """Writes newline-separated files of the source and target sequences."""
    for label in class_labels:
        with open(model_path + 'training_data/' + label + '_resume_' + str(resume_number)  + '.txt', 'w') as output_file:
            output_file.write('\n'.join(entry for entry in data[label]))

def get_extension(model_name, all_files):
    """Finds all folders that match model_name + '_' + int and returns
    the max of all present suffixes."""
    potentially_conflicting = [name for name in all_files if name.startswith(model_name)]
    extensions = [0]
    for name in potentially_conflicting:
        model_name_and_extension = name.split(model_name)
        if len(model_name_and_extension) == 2:
            extension_elements = model_name_and_extension[1].split('_')
            if len(extension_elements) == 2:
                if re.match("^[-+]?[0-9]+$", extension_elements[1]):
                    extensions.append(int(extension_elements[1]))
    return max(extensions) + 1

def prepare_model_folder(model_name, mode, training_mode, save_model, class_labels, data, models_parent_folder):
    """Creates folders for the model checkpoint and tensorboard data if they do not exist yet,
    creates/updates the resume tracker, saves the training data, and returns the model's name,
    the model's path, the checkpoints' path, the tensorboard summary path, the resume tracking
    number, and whether new directories have been created or not."""
    model_path = models_parent_folder + model_name + "/"
    create_directories = False
    if training_mode == 'resume':
        resume = True
    else:
        resume = False
    resume_number = 0 # In case training of an existing model is resumed, this will be adjusted later.
    # We have to create directories in the following case:
    if mode == "Train" and save_model:
        create_directories = True
        if resume and os.path.isdir(model_path): # If we resume the training of an existing model, we do not have to create the directories
            create_directories = False
    if create_directories:
        if os.path.isdir(model_path): # Check if a model 'model_name' exists, and prepare a suffix to prevent overwriting
            all_files = os.listdir(models_parent_folder)
            new_extension = get_extension(model_name, all_files)
            new_model_path = model_path[0:-1] + '_' + str(new_extension) + "/"
            model_name = model_name + '_' + str(new_extension)
            print("\nA model with name " + model_path[0:-1] + " already exists.")
            print("Your model will be saved in " + new_model_path + "\n")
            model_path = new_model_path
        # Create directories
        os.mkdir(model_path)
        os.mkdir(model_path + 'checkpoints/')
        os.mkdir(model_path + '/training_data/')
        os.mkdir(model_path + '/training_configs/')
        os.mkdir(model_path + '/plots/')
        os.mkdir(model_path + '/timing/')
        os.mkdir(model_path + '/tensorboard_data/')
        with open(model_path + 'training_resumes_tracker.txt', 'w') as output_file:
            output_file.write("Resume #\n" + str(resume_number))
    if mode == "Train" and resume and save_model and not create_directories: # This means we are resume training of an existing model.
        with open(model_path + 'training_resumes_tracker.txt', 'r') as resume_tracker_file: # Get resume number...
            resume_tracker_file_content = resume_tracker_file.readlines()
        resume_number = int(resume_tracker_file_content[-1]) + 1
        with open(model_path + 'training_resumes_tracker.txt', 'a') as output_file: # ...and write the next one down.
            output_file.write("\n" + str(resume_number))
    if mode == "Train" and save_model:
        write_files_with_training_data(model_path, class_labels, data, resume_number)
    checkpoints_path = model_path + 'checkpoints/'
    tensor_board_summary_path = model_path + 'tensorboard_data/tensorboard_data_resume_' + str(resume_number)
    saved_model_parameters_path = model_path + model_name + '_saved_model_parameters.yaml'
    return model_name, model_path, resume_number, checkpoints_path, tensor_board_summary_path, saved_model_parameters_path

def save_model_parameters(model_path, model_name, resume_number, training_parameters, model_parameters):
    with open(model_path + 'training_configs/' + model_name + '_saved_training_parameters_resume_' + str(resume_number) + '.yaml', 'w') as outfile:
        outfile.write('# This is an automatically generated file, do not change!\n')
    with open(model_path + 'training_configs/' + model_name + '_saved_training_parameters_resume_' + str(resume_number) + '.yaml', 'a') as outfile:
        yaml.dump(training_parameters, outfile, default_flow_style=False)
    if resume_number == 0:
        with open(model_path + model_name + '_saved_model_parameters.yaml', 'w') as outfile:
            outfile.write('# This is an automatically generated file, do not change!\n')
        with open(model_path + model_name + '_saved_model_parameters.yaml', 'a') as outfile:
            yaml.dump(model_parameters, outfile, default_flow_style=False)


if __name__ == '__main__':

    pass
