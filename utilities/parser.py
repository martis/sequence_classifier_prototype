import yaml

# We need this to parse boolean arguments
# https://stackoverflow.com/a/43357954/5441218
def str2bool(v):
    """Converts common command line strings for boolean type
    arguments to boolean."""
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def read_fasta_file(path, to_list=False):
    """Returns the contents of a FASTA file as a dictionary
    with sequence name as key and sequence as value."""
    with open(path, 'r') as fasta_file:
        fasta_file_content = fasta_file.readlines()
    sequences = {}
    for line in fasta_file_content:
        line = line.rstrip()
        if line.startswith('>'):
            name = line[1:]
        else:
            sequences[name] = line
    if to_list:
        sequences = [sequences[key] for key in sequences]
    return sequences

def read_newline_separated(path):
    """Returns the contents of a newline-separated file as a list."""
    with open(path, 'r') as file:
        file_content = file.readlines()
    return [line.rstrip() for line in file_content]

# Get the data
def get_data(mode, data_path, data_format, output_format):
    if mode == 'Train' or mode == 'Test':
        with open(data_path, 'r') as stream:
            class_data = yaml.safe_load(stream)
            classes_paths_dictionary = class_data['datasets']
            class_labels = [label for label in classes_paths_dictionary] # These are the actual class names
            if data_format == 'FASTA':
                data = {label : read_fasta_file(class_data['datasets'][label], to_list=True) for label in class_labels}
            else:
                data = {label : read_newline_separated(class_data['datasets'][label]) for label in class_labels}
    elif mode == 'Infer':
        if data_format == 'FASTA':
            data = read_fasta_file(data_path)
        else:
            data = read_newline_separated(data_path)
            data = {'s' + str(i) : data[i] for i in range(len(data))} # We introduce artificial sequence labels
        class_labels = None # We will load them later
        if output_format is None:
            output_format = data_format
    return data, class_labels, output_format


if __name__ == '__main__':

    pass
